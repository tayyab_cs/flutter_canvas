import 'package:flutter/material.dart';

class Shapes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
      aspectRatio: 1.0,
      child: new Container(
        padding: EdgeInsets.all(20.0),
        width: double.infinity,
        child: new CustomPaint(
          painter: MyPainter(),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  final _paint;
  MyPainter() : _paint = new Paint() {
    _paint.color = Color(0xffaecbf7);
    _paint.strokeCap = StrokeCap.square;
    _paint.strokeWidth = 10.0;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(Offset(25.0, 100.0), Offset(25.0, 300.0), _paint);

    _paint.color = Color(0xFF0062F7);
    canvas.drawLine(Offset(25.0, 100.0), Offset(300.0, 100.0), _paint);

    var rect = new Rect.fromLTRB(140.0, 130.0, 80.0, 300.0);
    _paint.color = Color(0xFF92AEFF);
    canvas.drawRect(rect, _paint);
    canvas.drawCircle(Offset(107.0, 65.0), 30.0, _paint);

    _paint.color = Color(0xFFFF8080);
    _paint.strokeCap = StrokeCap.square;
    canvas.drawLine(Offset(145.0, 185.0), Offset(300.0, 185.0), _paint);

    // var gradient = new LinearGradient(
    //   // Where the linear gradient begins and ends
    //   begin: Alignment.topRight,
    //   end: Alignment.bottomLeft,
    //   // Add one stop for each color. Stops should increase from 0 to 1
    //   stops: [0.1, 0.5, 0.7, 0.9],
    //   colors: [
    //     // Colors are easy thanks to Flutter's Colors class.
    //     Colors.red[800],
    //     Colors.red[700],
    //     Colors.red[600],
    //     Colors.red[400],
    //   ],
    // );

    var gradient = RadialGradient(
      center: const Alignment(0.7, -0.6),
      radius: 0.2,
      colors: [const Color(0xFFFFFF00), const Color(0xFF0099FF)],
      stops: [0.4, 1.0],
    );

    Size s = new Size(100.0, 100.0);
    var rects = Offset(150.0, 200.0) & s;
    _paint.color = Color(0xFFFF0000);
    canvas.drawRect(
      rects,
      _paint..shader = gradient.createShader(rect),
    );

    var arcRect = Offset(57.0, 18.0) & s;
    _paint.color = Color(0xFFFF0000);
    _paint.style = PaintingStyle.stroke;
    canvas.drawArc(arcRect, 70.0, 300.0, null, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return null;
  }
}

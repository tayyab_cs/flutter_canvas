import 'package:flutter/widgets.dart';

class ShapeLandscape extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
      aspectRatio: 1.0,
      child: new Container(
        padding: EdgeInsets.all(20.0),
        width: double.infinity,
        child: new CustomPaint(
          painter: new ShapePainter(),
        ),
      ),
    );
  }
}

class ShapePainter extends CustomPainter {
  final _paint;
  ShapePainter() : _paint = new Paint() {
    _paint.color = Color(0xffaecbf7);
    _paint.strokeCap = StrokeCap.round;
    _paint.strokeWidth = 10.0;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(
      Offset(25.0, 100.0),
      Offset(25.0, 300.0),
      _paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return null;
  }
}

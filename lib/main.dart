import 'package:flutter/material.dart';
import 'package:flutter_canvas/shapes.dart';
import 'package:flutter_canvas/shapes_land.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(HomePage());

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Canvas',
      home: new MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  Widget _layoutDetails(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Shapes();
    } else {
      return ShapeLandscape();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(FontAwesomeIcons.paintRoller),
          onPressed: () {
            // Do Something.
          },
        ),
        title: new Container(
          alignment: Alignment.center,
          child: new Text('Canvas'),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              FontAwesomeIcons.paintBrush,
              size: 20.0,
              color: Colors.white,
            ),
            onPressed: null,
          ),
        ],
      ),
      body: _layoutDetails(context),
    );
  }
}
